from random import randint
import time
import redis
import traceback
import json
import sys


class RedisConnector(object):

    def __init__(self, channel="demo"):

        try:
            self.pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
            self.redis_server = redis.Redis(connection_pool=self.pool)
            self.tweet_q = 'tweet_queue'
            self.CHANNEL = channel
            self.pub_sub_client = redis.StrictRedis()
        except redis.ConnectionError:
            traceback.print_exc()
            g.logger.error("Could not connect to Redis...:{0}".format(redis.ConnectionError))

    def get_variable(self, variable_name):
        response = self.redis_server.get(variable_name)
        return response

    def set_variable(self, variable_name, variable_value):
        self.redis_server = redis.Redis(connection_pool=self.pool)
        self.redis_server.set(variable_name, variable_value)

    def store_tweet_first_in_q(self, tweet):
        redis.Redis.lpush(self.redis_server, self.tweet_q, tweet)

    def send_message(self, message):
       message = message.encode('utf8')
       return self.pub_sub_client.publish(self.CHANNEL, message)

    def receive_message(self):
        # client = redis.StrictRedis()
        # pubsub = client.pubsub()
        self.pub_sub = self.pub_sub_client.pubsub()
        self.pub_sub.subscribe(self.CHANNEL)
        message = ""
        for event in self.pub_sub.listen():
            if event['type'] == 'message':
                message = event['data'].decode('utf8')
                break
        self.pub_sub.unsubscribe()

        return message
    def get_tweet_last_in_q(self):
        return redis.Redis.rpop(self.redis_server, self.tweet_q)

    def get_tweet_q_len(self):
        return redis.Redis.llen(self.redis_server,self.tweet_q)


def random_redis_pub_sub(sleep=5):
    tweets = [{"msg_type":"total_count","msg":{"total_count":250, "date_time": "26/2/2016 11:35"}},
              {"msg_type":"total_count","msg":{"total_count":520, "date_time": "26/2/2016 11:45"}},
              {"msg_type":"total_count","msg":{"total_count":880, "date_time": "26/2/2016 11:59"}},
              {"msg_type":"count","msg":{"positive":250, "negative":250,"neutral":250,"date_time": "26/2/2016 11:35"}},
              {"msg_type":"count","msg":{"positive":520,"negative":520,"neutral":520, "date_time": "26/2/2016 11:45"}},
              {"msg_type":"count","msg":{"positive":880, "negative":520,"neutral":520,"date_time": "26/2/2016 11:59"}},
              {"msg_type":"tweet","msg":{"tweet":"@stellargirl I loooooooovvvvvveee my Kindle2. Not that the DX is cool, but the 2 is fantastic in its own right.", "emotion": "positive"}},
              {"msg_type":"tweet","msg":{"tweet":"Reading my kindle2...  Love it... Lee childs is good read.","emotion": "positive"}},
              {"msg_type":"tweet","msg":{"tweet":"Ok, first assesment of the #kindle2 ...it fucking rocks!!!","emotion": "positive"}},
              {"msg_type":"tweet","msg":{"tweet":"@kenburbary You'll love your Kindle2. I've had mine for a few months and never looked back. The new big one is huge! No need for remorse! :)","emotion": "positive"}},
              {"msg_type":"tweet","msg":{"tweet":"@mikefish  Fair enough. But i have the Kindle2 and I think it's perfect  :)","emotion": "positive"}},
              {"msg_type":"tweet","msg":{"tweet":"@richardebaker no. it is too big. I'm quite happy with the Kindle2.","emotion": "positive"}},
              {"msg_type":"tweet","msg":{"tweet":"Fuck this economy. I hate aig and their non loan given asses.","emotion": "positive"}},
              {"msg_type":"tweet","msg":{"tweet":"Jquery is my new best friend.","emotion": "positive"}},
              {"msg_type":"tweet","msg":{"tweet":"Loves twitter","emotion": "positive"}},
              {"msg_type":"tweet","msg":{"tweet":"how can you not love Obama? he makes jokes about himself.","emotion": "positive"}},
              {"msg_type":"total_count","msg":{"total_count":10, "date_time": "26/2/2016 11:29"}},
              {"msg_type":"total_count","msg":{"total_count":101, "date_time": "26/2/2016 11:32"}},
              {"msg_type":"total_count","msg":{"total_count":250, "date_time": "26/2/2016 11:35"}},
              {"msg_type":"total_count","msg":{"total_count":520, "date_time": "26/2/2016 11:45"}},
              {"msg_type":"total_count","msg":{"total_count":880, "date_time": "26/2/2016 11:59"}},
              {"msg_type":"tweet","msg":{"tweet":"how can you not love Obama? he makes jokes about himself.","emotion": "positive"}},
              {"msg_type":"tweet","msg":{"tweet":"how can you not love Obama? he makes jokes about himself.","emotion": "positive"}},
            ]

    try:
        while True:
            # for i in range(0, len(tweets), 1):
            i = randint(0, len(tweets)-1)
            redis_client.send_message(json.dumps(tweets[i]))
            time.sleep(sleep)
    except KeyboardInterrupt:
        print 'interrupted!'

if __name__ == "__main__":
    sleep = 5
    channel = "demo"

    if len(sys.argv) == 2:
        sleep = sys.argv[0]
        channel = sys.argv[1]

    redis_client = RedisConnector(channel)
    random_redis_pub_sub(sleep)